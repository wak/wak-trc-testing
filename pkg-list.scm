;; Copyright (C) 2010 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.

;; You should have received a copy of the GNU Lesser General Public
;; License along with this library. If not, see
;; <http://www.gnu.org/licenses/>.

(package (wak-trc-testing (0) (2009 9 5) (1))
  (depends (srfi-8)
           (srfi-9)
           (srfi-13)
           (srfi-39)
           (wak-common)
           (wak-syn-param)
           (wak-fmt)
           (wak-foof-loop))
  
  (synopsis "simple testing facility")
  (description
   "TRC-Testing is a simple, portable testing utility for Scheme. It makes no"
   "pretenses to be a comprehensive framework for building complex test"
   "suites; it is merely a simple tool to express simple test suites, which"
   "requires very little cognitive overhead.")
  (homepage "http://mumble.net/~campbell/darcs/trc-testing/")
  
  (libraries
   (sls -> "wak")
   (("trc-testing" "private") -> ("wak" "trc-testing" "private"))))

;; Local Variables:
;; scheme-indent-styles: (pkg-list)
;; End:
