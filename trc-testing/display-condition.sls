#!r6rs
;;; display-condition.sls --- Pretty-printer for conditions

;; Copyright (C) 2011 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.

;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.

;; You should have received a copy of the GNU Lesser General Public
;; License along with this library. If not, see
;; <http://www.gnu.org/licenses/>.

(library (wak trc-testing display-condition)
  (export display-condition)
  (import (rnrs)
          (wak foof-loop)
          (wak fmt))
  
(define-syntax formatting
  (syntax-rules ()
    ((_ (state-var) (formatter state-expr) cont . env)
     (cont
      ()                                         ;Outer bindings
      ((state-var state-expr                     ;Loop variables
                  (formatter state-expr)))
      ()                                         ;Entry bindings
      ()                                         ;Termination conditions
      ()                                         ;Body bindings 
      ()                                         ;Final bindings
      . env))))

(define (dsp-simple-condition c)
  (define (dsp-rtd rtd rtd.fields-list n-fields)
    (cat (record-type-name rtd)
         (case n-fields
           ((0) fmt-null)
           ((1)
            (cat ": " (wrt/unshared
                       ((record-accessor (caar rtd.fields-list) 0) c))))
           (else
            (cat ":\n"
                 (fmt-join
                  (lambda (rtd.fields)
                    (dsp-fields (car rtd.fields) (cdr rtd.fields)))
                  rtd.fields-list))))
         nl))
  (define (dsp-fields rtd fields)
    (lambda (st)
      (loop ((for i (up-from 0 (to (vector-length fields))))
             (for st (formatting
                      (cat "      "
                           (vector-ref fields i) ": "
                           (wrt/unshared ((record-accessor rtd i) c)) "\n")
                      st)))
        => st)))
  (lambda (st)
    (let ((c-rtd (record-rtd c)))
      (let loop ((rtd c-rtd)
                 (rtd.fields-list '())
                 (total-fields 0))
        (if rtd
            (let* ((field-names (record-type-field-names rtd))
                   (n-fields (vector-length field-names)))
              (loop (record-type-parent rtd)
                    (if (zero? n-fields)
                        rtd.fields-list
                        (cons (cons rtd field-names) rtd.fields-list))
                    (+ total-fields n-fields)))
            ((dsp-rtd c-rtd rtd.fields-list total-fields) st))))))

(define (dsp-condition c)
  (define (dsp-components components)
    (lambda (st)
      (loop ((for c (in-list components))
             (for i (up-from 1))
             (for st (formatting
                      (cat "  " i ". " (dsp-simple-condition c))
                      st)))
        => st)))
  (cond
    ((condition? c)
     (let ((components (simple-conditions c)))
       (if (null? components)
           (dsp "Condition object with no further information\n")
           (cat "Condition components:\n"
                (dsp-components components)))))
    (else
     (cat "Non-condition object: " c (wrt/unshared c) "\n"))))

(define display-condition
  (case-lambda
    ((c port)
     (fmt port (fmt-columns (list (lambda (line) (cat " " line))
                                  (dsp-condition c)))))
    ((c)
     (display-condition c (current-output-port)))))

)
